---
layout: post
title: "End of a potential era"
---

I've been debating whether or not to close this site (1 more week left on domain renewal) given that every government in the world has started overreacting over the common cold/flu and holding the world hostage.

Also have not been travelling (apart from domestic travels for the last year), and even then it can be quite problematic when travels get disrupted. Luckily I do have a laptop and can work from almost everywhere. And most cities I go to have similar ameneties. It also helps me in not overpacking and being as minimalistic as possible (lugging stuff onto a small speedboat or motorcycle is something you want to avoid less often).

Most likely I'll keep this site up for posterity purposes for a while.

We will hopefully get through this and roam the planet free again.

Happy Cinco de Mayo 2021, and stay healthy everyone and wash your hands!
